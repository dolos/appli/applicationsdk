open module applicationsdk {
	exports fr.dolos.sdk.commands;
	exports fr.dolos.sdk.network;
	exports fr.dolos.sdk.events;
	exports fr.dolos.sdk;
	exports fr.dolos.sdk.modules;
	exports fr.dolos.sdk.models;
	exports fr.dolos.sdk.exceptions;

	requires transitive javafx.controls;
}