package fr.dolos.sdk;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

public interface ThreadPool {

    public <T> Future<T> runAsync(Callable<T> callable);

    public <T> Future<T> runOnDedicated(Callable<T> callable);

    public <T> ScheduledFuture<T> runDelayed(Callable<T> runnable, long delay);

    public ScheduledFuture<?> runTimed(Runnable runnable, long delay, long timer);

    public void runOnCoreThread(final Runnable runnable);
}
