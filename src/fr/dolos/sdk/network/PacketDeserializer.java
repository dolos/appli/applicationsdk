package fr.dolos.sdk.network;

public interface PacketDeserializer {

    public Packet deserialize(String packet, String data);
}
