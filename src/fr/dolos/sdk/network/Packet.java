package fr.dolos.sdk.network;

public interface Packet {

    public String getName();

    public String serialize();
}
