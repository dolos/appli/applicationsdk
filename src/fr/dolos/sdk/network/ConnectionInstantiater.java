package fr.dolos.sdk.network;

import java.util.concurrent.Future;

public interface ConnectionInstantiater {

    public Future<NetworkClient> connectTo(String ip, int port, int id);

    public void setPacketInstantiater(PacketInstantiater instantiater);

    public void setFilterApplicative(Filter filter);

    public void setConnectionListener(ConnectionListener listener);
}
