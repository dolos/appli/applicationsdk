package fr.dolos.sdk.network;

public interface PacketInstantiater {

    public Packet instantiate(String packet, String data);

    public void onPacketReady(Packet p, NetworkClient client);
}
