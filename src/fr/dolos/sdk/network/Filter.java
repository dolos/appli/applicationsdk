package fr.dolos.sdk.network;

public interface Filter {

    public <T extends Packet> void onPacketSending(T packet);

    public void onPacketReceiving(Packet packet);

    public String onDataSending(String data);

    public String onDataReceiving(String data);
}
