package fr.dolos.sdk.network;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PacketHandler {

    public enum HandlerPriority {
        LOWEST,
        LOW,
        HIGH,
        NORMAL,
        HIGHEST
    }

    HandlerPriority priority() default HandlerPriority.NORMAL;
}
