package fr.dolos.sdk.network;

import java.io.IOException;

public interface NetworkClient {

    public void close();

    public boolean isConnected();

    public <T extends Packet> void send(T packet) throws IOException;
    
    public String getIp();
}
