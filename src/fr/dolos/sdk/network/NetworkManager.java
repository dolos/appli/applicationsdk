package fr.dolos.sdk.network;

import java.util.concurrent.Future;

/**
 * NetworkManager is used to handle connection and data manipulation
 * @author Mathias Hyrondelle
 */
public interface NetworkManager {

    /**
     * Initialize connection to a drone
     * @param ip Drone ip
     * @param port Drone port
     * @return Future representing the pending connection. Once the future is complete, <br>
     * the connection should be done. May return null if there is no NetworkModule set
     */
    public Future<NetworkClient> connectTo(String ip, int port, int id);

    /**
     * Add a filter that will be called each time a data is sent
     * @param filter 
     */
    public void addFilter(Filter filter);

    /**
     * Remove a filter
     * @param filter 
     */
    public void removeFilter(Filter filter);

    /**
     * Register a packet deserializer used to deserialize received data
     * @param packetName Packet name to deserialize
     * @param deserializer The deserializer to use
     */
    public void registerDeserializer(String packetName, PacketDeserializer deserializer);

    /**
     * Register a packet receiver
     * @param o Object receiving data. This object must use the @PacketHandler annotation to handle packets
     */
    public void registerReceiver(Object o);

    /**
     * Remove a packet receiver
     * @param o Receiver to remove
     */
    public void removeReceiver(Object o);
}
