package fr.dolos.sdk.network;

import fr.dolos.sdk.models.Drone;

public interface ConnectionListener {

    public void onClientReady(Drone client);

    public void onClientDisconnected(Drone client);
}
