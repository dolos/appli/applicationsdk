package fr.dolos.sdk.modules;

import fr.dolos.sdk.Core;

/**
 * @author Mathias
 */
public abstract class SystemModule extends Module {

    public abstract boolean load(Core core, SystemManager manager);

    @Override
    public ModuleType type() {
        return (ModuleType.SYSTEM);
    }
}
