package fr.dolos.sdk.modules;

/**
 * @author Mathias
 */
public enum ModuleType {

    SYSTEM,
    USER
}
