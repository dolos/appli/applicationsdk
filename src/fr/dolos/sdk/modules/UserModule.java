package fr.dolos.sdk.modules;

import fr.dolos.sdk.Core;

/**
 * @author Mathias
 */
public abstract class UserModule extends Module {

    public abstract boolean load(Core core);

    @Override
    public ModuleType type() {
        return (ModuleType.USER);
    }
}
