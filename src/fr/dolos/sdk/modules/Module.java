package fr.dolos.sdk.modules;

import java.util.List;

import fr.dolos.sdk.Core;
import javafx.scene.control.Tab;

/**
 * @author Mathias
 */
public abstract class Module {

    public boolean preload() {
        return true;
    }
    
    public abstract void unload();

    public abstract String getName();

    public List<String> dependencies() {
        return null;
    }

    public abstract ModuleType type();

    public void update() {
    }

    protected Core getCore() {
        return (Core.getInstance());
    }
    
    protected Tab newTab(String name)
    {
    	return Core.getInstance().getUIManager().createTab(name);
    }
}
