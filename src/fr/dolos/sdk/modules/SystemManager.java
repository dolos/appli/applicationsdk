package fr.dolos.sdk.modules;

import fr.dolos.sdk.network.ConnectionInstantiater;

public interface SystemManager {

    public void setConnectionInstantiater(ConnectionInstantiater instantiater);
    public ConnectionInstantiater getConnectionInstantiater();
}
