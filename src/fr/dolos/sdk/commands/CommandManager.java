package fr.dolos.sdk.commands;

import fr.dolos.sdk.exceptions.CommandNotFoundException;

/**
 * CommandManager used to register and handle console commands <br/>
 * Commands can also be executed manually
 * @author Mathias
 */
public abstract class CommandManager {
    
    public void registerHandler(DolosCommand cmd, CommandHandler handler)
    {
        registerHandler(cmd.label, handler);
    }
    public abstract void registerHandler(String label, CommandHandler handler);
    public abstract void readCommand();
    public abstract void executeCommand(String command, String[] args) throws CommandNotFoundException;
}
