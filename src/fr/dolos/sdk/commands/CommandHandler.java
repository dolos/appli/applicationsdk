package fr.dolos.sdk.commands;

public interface CommandHandler {

    public void onCommand(String label, String[] args);
}
