package fr.dolos.sdk.commands;

/**
 * Enumeration containing all default commands for Dolos
 * @author Mathias
 */
public enum DolosCommand {
    STOP("stop"),
    CONNECT("connect"),
    ADD_WAYPOINT("add_waypoint");
    
    public final String label;

    private DolosCommand(String label) {
        this.label = label;
    }
}
