package fr.dolos.sdk.events;

/**
 * @author Mathias
 */
public interface EventManager {

    /**
     * Post an event to the event system
     *
     * @param event
     */
    public <T> void post(T event);

    /**
     * Register a new event handler
     *
     * @param o
     * @return integer representing the event handler
     */
    public void registerEventHandler(Object o);

    /**
     * Remove an Event Handler
     *
     * @param eh EventHandler id
     */
    public void removeEventHandler(Object eh);
}
