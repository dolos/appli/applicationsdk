package fr.dolos.sdk.events;

import fr.dolos.sdk.models.Drone;

public class ClientConnectedEvent {

    private final Drone client;

    public ClientConnectedEvent(Drone client) {
        this.client = client;
    }

    public Drone getClient() {
        return (client);
    }
}
