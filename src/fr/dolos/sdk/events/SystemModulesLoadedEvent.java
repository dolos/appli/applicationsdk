package fr.dolos.sdk.events;

/**
 * Event thrown when system modules are successfully loaded
 *
 * @author Mathias
 *
 */
public class SystemModulesLoadedEvent {
}
