package fr.dolos.sdk.events;

import fr.dolos.sdk.models.Drone;

public class ClientDisconnectedEvent {

    private final Drone client;

    public ClientDisconnectedEvent(Drone client) {
        this.client = client;
    }

    public Drone getClient() {
        return (client);
    }
}
