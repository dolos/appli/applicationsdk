package fr.dolos.sdk.events;

import fr.dolos.sdk.network.NetworkClient;

public class PacketReceivedEvent<T> {

    public T p;
    public NetworkClient client;

    public PacketReceivedEvent(T p, NetworkClient client) {
        this.p = p;
        this.client = client;
    }
}
