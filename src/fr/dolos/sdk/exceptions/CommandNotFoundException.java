/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.dolos.sdk.exceptions;

/**
 *
 * @author Mathias
 */
public class CommandNotFoundException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 5928958964587552796L;
	private static final String CMD_ERROR_MSG = "Command %s is not registered";
    
    public CommandNotFoundException()
    {
        super();
    }
    
    public CommandNotFoundException(String commandName)
    {
        super(String.format(CMD_ERROR_MSG, commandName));
    }
}
