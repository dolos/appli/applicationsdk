package fr.dolos.sdk.exceptions;

public class NoEventHandlersAnnotated extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8310923662913996001L;

    public NoEventHandlersAnnotated() {
        super("No event handlers annotated on registered event handler");
    }

    public NoEventHandlersAnnotated(String msg) {
        super(msg);
    }
}
