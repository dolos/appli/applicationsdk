
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.dolos.sdk;

import javafx.scene.control.*;
import javafx.scene.layout.Pane;

/**
 * UIManager allowing modules to interact with user interface
 * @author Mathias Hyrondelle
 */
public interface UIManager {
    
	/**
	 * Create a tab
	 * @param moduleName Tab name
	 * @return Tab
	 */
    public Tab createTab(String moduleName);
    /**
     * Bind pane height and width to the available space for tabs
     * @param pane Pane to bind
     */
    public void bindWidthAndEight(Pane pane);
}
