package fr.dolos.sdk;

import fr.dolos.sdk.commands.CommandManager;
import fr.dolos.sdk.events.EventManager;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.network.NetworkManager;
import javafx.collections.ObservableList;

/**
 * @author Mathias
 */
public abstract class Core {

    private static Core instance = null;

    /**
     * @return Core instance
     */
    public static Core getInstance() {
        return (instance);
    }

    /**
     * Used to set the core instance
     *
     * @param core The ICore implementation
     */
    public static void setInstance(Core core) {
        instance = core;
    }
    
    /**
     * @return EventManaging system
     */
    public abstract EventManager getEventManager();

    public abstract NetworkManager getNetworkManager();

    public abstract ThreadPool getScheduler();
    
    public abstract CommandManager getCommandManager();

    public abstract void runOnCoreThread(Runnable runnable);
    
    public abstract UIManager getUIManager();

    public abstract ObservableList<Drone> getDroneList();
   
}
