package fr.dolos.sdk.models;

public class Position {
	public double latitude;
	public double longitude;
	public double altitude;
	
	public Position() {
		latitude = 0;
		longitude = 0;
		altitude = 0;
	}
	
	public Position(double latitude, double longitude, double altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}
}
