package fr.dolos.sdk.models;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import fr.dolos.sdk.Core;
import fr.dolos.sdk.network.NetworkClient;
import javafx.application.Platform;
import javafx.beans.value.ObservableValueBase;

public class Drone {
	
	public static class PositionProperty extends ObservableValueBase<Position>
	{
		private Position position;
		
		public PositionProperty() {
			position = new Position();
		};
		public PositionProperty(Position pos)
		{
			this.position = pos;
		}
		
		public void setPosition(Position pos)
		{
			this.position = pos;
			if (!Platform.isFxApplicationThread())
				Platform.runLater(() -> this.fireValueChangedEvent());
			else
				this.fireValueChangedEvent();
		}
		
		@Override
		public Position getValue() {
			return position;
		}
		
	}
	
	private volatile PositionProperty position;
	private NetworkClient client = null;
	private final String ip;
	private final int id, port;
	private final ConcurrentHashMap<String, Object> data;
	
	public Drone()
	{
		id = 0;
		ip = "127.0.0.1";
		port = 3204;
		data = new ConcurrentHashMap<>();
		position = new PositionProperty();
	}
	
	public Drone(int id, String ip, int port)
	{
		this.data = new ConcurrentHashMap<>();
		this.position = new PositionProperty();
		this.client = null;
		this.ip = ip;
		this.id = id;
		this.port = port;
	}
	
	public Drone(int id, String ip, int port, Position p)
	{
		this(id, ip, port);
		this.position = new PositionProperty(p);
	}
	
	public Drone(NetworkClient client, int id, int port, String ip)
	{
		this(id, ip, port);
		this.client = client;
	}
	
	public String getIp() { return ip; }
	public int getId() { return id; }
	public int getPort() { return port; }

    public synchronized NetworkClient getConnection()
    {
    	if (client == null)
			try {
				client = Core.getInstance().getNetworkManager().connectTo(getIp(), getPort(), id).get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				return null;
			}
    	return client;	
    }

    public PositionProperty positionProperty() { return position; }
    public void setPosition(Position p)
    {
    	positionProperty().setPosition(p);
    }
    public Position getPosition()
    {
    	return positionProperty().getValue();
    }
    public ConcurrentHashMap<String, Object> getData() { return data; }
}
